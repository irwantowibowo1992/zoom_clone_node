### Menampilkan Video Ke Dalam Halaman Web

ID room sudah kita dapatkan, dan sekarang saatnya kita berikan kode untuk menampilkan videonya.

Untuk kodenya kurang lebih seperti ini :
```
	const videoGrid = document.getElementById('video-grid');
	const myVideo = document.createElement('video');
	myVideo.muted = true;

	let myVideoStream
	navigator.mediaDevices.getUserMedia({
		video: true,
		audio: true
	}).then(stream => {
		myVideoStream = stream;
		addVideoStream(myVideo, stream);
	})

	const addVideoStream = (video, stream) => {
		video.srcObject = stream;
		video.addEventListener('loadedmetadata', () => {
			video.play();
		})
		videoGrid.append(video);
	}
```

Kemudian kita buat sebuah div untuk tempah video kita ditampilkan :
```
	<div id="video-grid">

	</div>
```

Jangan lupa juga untuk memindahkan script memanggil file javascript kita (`<script src="script.js"></script>`) ke bagian paling bawah body.
Hal ini dilakukan supaya saat pemanggilan elemen id `video-grid` (`const videoGrid = document.getElementById('video-grid')` di file `script.js`) hasilnya tidak null.
